    <%-- 
    Document   : edit
    Created on : 18-04-2020, 23:29:14
    Author     : Diablo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="root.model.entities.Testin" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
         <br> </br>
        <h1 id="titulo_edit">Edicion de Usuarios</h1>
         <br> </br>
        <form action="controller" method="POST">
            ID:
            <input type="text" name="txtid" value="${usuario.getId()}" readonly/><br><br>
            Nombre:
            <input type="text" name="txtnombre" value="${usuario.getNombre()}" /><br><br>
            Apellido:
            <input type="text" name="txtapellido" value="${usuario.getApellido()}" /><br><br>
            Direccion:
            <input type="text" name="txtdireccion" value="${usuario.getDireccion()}" /><br><br>
            Mail:
            <input type="text" name="txtmail" value="${usuario.getMail()}" /><br><br>
            <input type="submit" value="Grabar" name="accion" />
        </form>
    </body>
</html>
