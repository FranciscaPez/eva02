/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Diablo
 */
@Entity
@Table(name = "testing")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Testin.findAll", query = "SELECT t FROM Testin t"),
    @NamedQuery(name = "Testin.findById", query = "SELECT t FROM Testin t WHERE t.id = :id"),
    @NamedQuery(name = "Testin.findByNombre", query = "SELECT t FROM Testin t WHERE t.nombre = :nombre"),
    @NamedQuery(name = "Testin.findByApellido", query = "SELECT t FROM Testin t WHERE t.apellido = :apellido"),
    @NamedQuery(name = "Testin.findByDireccion", query = "SELECT t FROM Testin t WHERE t.direccion = :direccion"),
    @NamedQuery(name = "Testin.findByMail", query = "SELECT t FROM Testin t WHERE t.mail = :mail")})
public class Testin implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "apellido")
    private String apellido;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 2147483647)
    @Column(name = "mail")
    private String mail;

    public Testin() {
    }

    public Testin(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Testin)) {
            return false;
        }
        Testin other = (Testin) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.forall.Testing[ id=" + id + " ]";
    }
    
}
