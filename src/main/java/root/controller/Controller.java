/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.entities.Testin;
import root.model.dao.TestinDAO;
import root.model.dao.exceptions.NonexistentEntityException;

/**
 *
 * @author Diablo
 */
@WebServlet(name = "Contoller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    TestinDAO dao = new TestinDAO();

    /**
     * Pro  cesses requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Contoller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Contoller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //declaracion de la variable accion
        String accion = request.getParameter("accion");

        // Accion de redireccion al formulario de creacion de registro
        if (accion.equalsIgnoreCase("Crear")) {
            request.getRequestDispatcher("add.jsp").forward(request, response);

            // creacion y guardado del usuario    
        } else if (accion.equalsIgnoreCase("Guardar")) {
            String nom = request.getParameter("nombre");
            String ape = request.getParameter("apellido");
            String dir = request.getParameter("direccion");
            String mai = request.getParameter("mail");

            Testin nuevo = new Testin();

            nuevo.setNombre(nom);
            nuevo.setApellido(ape);
            nuevo.setDireccion(dir);
            nuevo.setMail(mai);

            dao.create(nuevo);
            request.getRequestDispatcher("controller?accion=Listar").forward(request, response);

            // Listado de registros
        } else if (accion.equalsIgnoreCase("Listar")) {
            List<Testin> datos = dao.findTestinEntities();
            request.setAttribute("datos", datos);
            //request.getRequestDispatcher("controller?accion=Listar").forward(request, response);
            request.getRequestDispatcher("index.jsp?accion=Listar").forward(request, response);

            // Muestra de los datos del usuario
        } else if (accion.equalsIgnoreCase("Actualizar")) {
            int ide = Integer.parseInt(request.getParameter("id"));
            Testin user = dao.findTestin(ide);
            request.setAttribute("usuario", user);
            request.getRequestDispatcher("edit.jsp").forward(request, response);

            // Graba los cambios en el registro
        } else if (accion.equalsIgnoreCase("Grabar")) {
            Testin act = new Testin();

            act.setId(Integer.parseInt(request.getParameter("txtid")));
            act.setNombre(request.getParameter("txtnombre"));
            act.setApellido(request.getParameter("txtapellido"));
            act.setDireccion(request.getParameter("txtdireccion"));
            act.setMail(request.getParameter("txtmail"));

            EntityManagerFactory emf;
            emf = Persistence.createEntityManagerFactory("my_persistence_unit");
            TestinDAO con = new TestinDAO(emf);
            
            try {
                con.edit(act);
            } catch (Exception ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.getRequestDispatcher("controller?accion=Listar").forward(request, response);
            
            // Elimina un registro de la tabla
        }else if (accion.equalsIgnoreCase("Eliminar")) {
            int ide2 = Integer.parseInt(request.getParameter("id"));
            try {
                dao.destroy(ide2);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.getRequestDispatcher("controller?accion=Listar").forward(request, response);
        
        }else if (accion.equalsIgnoreCase("Cancelar")) {
            request.getRequestDispatcher("controller?accion=Listar").forward(request, response);
        }
        

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
