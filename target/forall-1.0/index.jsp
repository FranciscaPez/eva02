<%-- 
    Document   : index
    Created on : 18-04-2020, 21:23:36
    Author     : Diablo
--%>

<%@page import="root.model.dao.TestinDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="root.model.entities.Testin"%> 
<%@page import="root.controller.Controller"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>EVA 02 - Francisca Soto</title>
        <link href="css/estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <br> </br>
        <h1 id="titulo">Formulario de Registros de Usuarios</h1>
         <br>
                <form action="controller" method="POST">
            <input type="submit" value="Crear" name="accion" id="boton1"/>
            <input type="submit" value="Listar" name="accion" id="boton2" />
            <br>
            <br>
            <hr>
        </form>
         <br>
        <table border="1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Direccion</th>
                    <th>Mail</th>
                    <th>Accion</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="dato" items="${datos}">
                    <tr>
                        <th>${dato.getId()}</th>
                        <th>${dato.getNombre()}</th>
                        <th>${dato.getApellido()}</th>
                        <th>${dato.getDireccion()}</th>
                        <th>${dato.getMail()}</th>
                        <th>
                            <form action="controller" method="POST">
                                <input type="hidden" name="id" value="${dato.getId()}" />
                                <input type="submit" name="accion" value="Actualizar" id="boton3" />
                                <input type="submit" name="accion" value="Eliminar" id="boton4" />
                            </form>
                        </th>
                    </tr>
                </c:forEach>
            </tbody>
            

        </table>
                
    </body>
</html>
